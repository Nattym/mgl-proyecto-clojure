;;Calculadora
(import '(java.util Scanner))

(println "introduce el primer valor:")
(def scan (Scanner. *in*))
(def valor1(.nextDouble scan))

(println "introduce el operador:")
(def scan (Scanner. *in*))
(def operador(.nextLine scan))

(println "introduce el segundo valor:")
(def scan (Scanner. *in*))
(def valor2(.nextDouble scan))

(defn calculo [operador valor1 valor2]
	
     (if (= "*" operador)
     	     (* valor1 valor2)
     (if (= "-" operador)
     	     (- valor1 valor2) 
     (if (= "+" operador)
     	     (+ valor1 valor2) 
     (if (= "/" operador)
     	     (/ valor1 valor2)
     	     
     	     ))))
  )

(def resultado (calculo operador valor1 valor2))

(println  (concat ["Resultado:"] [resultado] ))


