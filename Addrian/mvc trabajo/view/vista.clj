(ns view.vista  )
(import '(java.util Scanner))
(def scan (Scanner. *in*))

(println "Leer 1)  -----  Agregar 2)")
(def opcion (.nextLine scan))
(def agregar "1")
(def leer "2")


(case opcion
  "1" (def a 1)
  "2" (def a 2)
  (str "else" opcion(println "opcion no valida") (def a 3)) 
)


(defn vista []

	(+ 0 a)
	
	)

(defn text []
(println "-- Marca -- Modelo -- Tamaño -- Potencia -- Impedancia --") 
(println "") 

    (print "Marca: ") 
	(def Marca (.nextLine scan))
	(print "Modelo: ") 
	(def Modelo (.nextLine scan))
	(print "Tamaño: ") 
	(def Tamano (.nextLine scan))
	(print "Potencia: ") 
	(def Potencia (.nextLine scan))
	(print "Impedancia") 
	(def Impedancia (.nextLine scan))

	(str Marca " , " Modelo " , " Tamano " , " Potencia " , " Impedancia)

)
