(import '(java.util Scanner))
(println  "Salario del empleado: ")
(def scan (Scanner. *in*))
(def salario(.nextInt scan))
 (if (<= salario 1000)
 	(def opcion (* salario 0.90)))
 (if (and (> salario 1001) (<= salario 2000))
 	(def opcion (* salario 0.85)))
 (if (> salario 2000)
 	(def opcion (* salario 0.82)))
 	(println "Salario total" opcion)
 	(println "descuento" (- salario opcion))
 (def perder(.nextInt scan))