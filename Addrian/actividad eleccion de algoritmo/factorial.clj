(import '(java.util Scanner))
(println  "Ingresar numero a calcular factorial ")
(def scan (Scanner. *in*))
(def Numero (.nextInt scan))

(defn factorial1 [n]
	(if (< n 2) 1
	(* n (factorial1 (dec n)))))

(println "el factorial de" Numero " es " (factorial1 Numero))
(def fin(.nextInt scan))



