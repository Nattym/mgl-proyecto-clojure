(import (java.util Scanner))
(def teclado (Scanner. *in*))
(def p 0)
(def in 0)
(def pos 0)
(def n 0)
(def numero 0)
(def incremento (atom 10)) 
(println "****************************************************************************")
(println "**  <Programa contador de números pares, impares, positivos y negativos> **|")
(println "****************************************************************************")
(println "")
(println ">############# <El programa recoje 50 número y luego los evalua> ##########<")
(println "")


(println "=> Ingrese los numeros:")

(while (pos? @incremento) 
	(do  
		(def numero (.nextInt teclado))
		(do (if (= (mod numero 2) 0) (def p (+ p 1)) (def in (+ in 1)))) 
		(if (>= numero 0) (def pos (+ pos 1)) (def n (+ n 1)))
		(swap! incremento dec)
	)
)



(println "")
(println "----------------------------------------------------------------------------+")
(println "=> Ingresaste:                                                             |")
(println "=> "p "numero(s) pare(s)                                                    |")
(println "=> "in "numero(s) impare(s)                                                  |")
(println "=> "pos "numero(s) posiivo(s)                                                |")
(println "=> " n "numero(s) negativo(s)                                                |")
(println "----------------------------------------------------------------------------+")

(println "")
(println "**************************************************************************")
(println "*************************      ->  FIN  <-    ***************************|")
(println "**************************************************************************")
(println "")
