(import '(java.util Scanner))
(def teclado (Scanner. *in*))
(println "---SEGUNDOS FALTANTES PARA EL MINUTO---")
(println "Introduce los segundos")
(def tiempo_seg (.nextInt teclado))

(if (>= (mod tiempo_seg 60) 0)
	(do 
		(def minu(quot tiempo_seg 60))
		(def minutos (+ minu 1))
		(def segundos (- 60 (mod tiempo_seg 60)))
		)
	)
(println "Segundos restantes:" segundos "seg." "para " minutos "minutos")